<?php
namespace KDA\Laravel\ProspectManager;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    protected $packageName ='laravel-prospect-manager';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/prospect-manager.php'  => 'kda.laravel-prospect-manager'
    ];
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
