<?php

namespace KDA\Laravel\ProspectManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;

class Prospect extends Model
{
    use HasFactory;
    use HasDefaultAttributes;
    protected $fillable = [
        
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];

    public function createDefaultAttributes(){
        $this->defaultAttribute('user_id',auth()->user()->id);
    }
    
    protected static function newFactory()
    {
        return  \KDA\Laravel\ProspectManager\Database\Factories\ProspectFactory::new();
    }

    public function contact(){
        return $this->morphTo();
    }

}
