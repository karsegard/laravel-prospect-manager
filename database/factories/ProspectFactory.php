<?php

namespace KDA\Laravel\ProspectManager\Database\Factories;

use KDA\Laravel\ProspectManager\Models\Prospect;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProspectFactory extends Factory
{
    protected $model = Prospect::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
